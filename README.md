# Korszerű elektronika irányítási módszerei - Projektfeladat
**[DC motor kaszkád fordulatszámszabályozás](/doc/feladatkiiras.pdf)**

## Tudáskút

**Szabályozás**
- [DC motor kaszkád szabályozás gyorsan és érthetően](https://www.youtube.com/watch?v=LdVyC8BOBjA)
- [DC motor kaszkád szabályozással (part 1)](https://www.youtube.com/watch?v=LnhpGBGuYBc)

**Átviteli függvények**
- [Folytonos --> diszkrét](https://en.wikipedia.org/wiki/Z-transform#Relationship_to_Laplace_transform)

## Roadmap

**Plant**
- [x] Simpowersystems szimuláció
    - [x] Motor
    - [x] H-híd
    - [x] Terhelőnyomaték
    - [x] Gerjesztés
- [x] Folytonos átviteli függvények
    - [x] Motor
    - [ ] H-híd (nem szükséges)
    - [x] Terhelőnyomaték
    - [x] Gerjesztés
- [ ] Diszkrét átviteli függvények
    - [ ] Motor
    - [ ] H-híd
    - [ ] Terhelőnyomaték
    - [ ] Gerjesztés

**Encoder - counter**
- [x] Encoder - decoder

**Control**
- [x] Szabályozók típusainak meghatározása
- [x] Szabályozók megépítése
- [x] P, I, D értékeinek meghatározása parametrikusan
- [ ] A H hídnak nem csak 2 állapota lehet
- [x] Gerjesztés controller

**Extra feladat (sajnos nem sikerült)**
- [ ] Fixpontossá alakítani