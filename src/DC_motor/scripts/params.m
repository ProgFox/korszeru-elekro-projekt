clc;

%PlantVariant = 1;       % 1: Simpowersystems
%                        % 2: CT_transfer_functions
                        
PWMCarrierVariant = 2; % 1: Up-Count
                       % 2: UpDown
                       
Deadtime = 2e-6;
dt = 100e-9;

Ug = 240;

%Omega PID szab�lyz�
P_omega = 4;
I_omega = 0.008;
D_omega = 15;
I_omega_aw = 1;

% Ir�ny�tott szakasz
L = 0.002866;
R = 0.2275;
tau = L/R;
R_g = 102.3;
L_g = 20.82;
tau_g = L_g/R_g;
J = 0.1239;
Laf = 0.401;

% �ram sk�l�z�s
In = 10;
Xn = 10;

Imax = 3*In;

% Teljes�tm�ny er�s�t� param�terek
Udc = 360;
A = R*In/Xn;
tau_A = 30e-6;

% Zavarkompenz�ci�
K_ub = 0.05;

% �ram�rz�kel�s
K_I = Xn/In;
tau_KI = 3e-6;

% Kapcsol��zem� beavatkoz�
fsw = 20e3;

Uv = 1;

% PI szab�lyoz�
Ts = 1/fsw/2;
phim = pi/3;
phi0 = pi/2 - phim;
Th = 2*Ts;
wc = (2/3)*phi0/Th;
TI = 1/wc/tan(phi0/3);
Tsys = L*Uv/(Udc*K_I);
Ap = wc*Tsys;

% Omega PI szab�lyz�
B = 15;
TI_omega = B^2 * TI;
Ap_omega = J/(B*TI);

% Sz�r�
T_RC = 0.001;

% Enk�der param�terek
F = 512;
